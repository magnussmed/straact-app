module.exports = ( env ) => {
  'use strict';

  const fs = require('fs');

  /*
    * Import project configuration.
    * This is where we define the project specific configuration for use across the webpack configuration.
    */
  const projectConfig = require('./src/assets/src/webpack/project.config')(env);

  // import config modules
  const configCommon = require('./src/assets/src/webpack/common.config')(env);

  // utility class
  const utils = require('./src/assets/src/webpack/utils/core');

  utils.logToConsole( env );

  return configCommon;
};
