import axios from 'axios';
import { APP_ROOT, API_ENDPOINTS, UID, ACCESS_TOKEN } from '../../utils/constants';

const getProject = ( project_id ) => {
	return async function getProjectThunk( dispatch ) {
		await axios.get(
			`${ API_ENDPOINTS.STRAACT_FLASK_API }/project/get`,
			{
				params: {
					uid: UID,
					project_id: project_id
				},
				headers: {
					"Authorization" : `Bearer ${ACCESS_TOKEN}`
				}
			}
		).then( response => {
			if ( response.data ) {
				if ( response.data.type === 'success' ) {
					dispatch({ type: 'GET_PROJECT', payload: response.data.project })
				}

				if ( response.data.type === 'error' ) {
					dispatch({ type: 'GET_PROJECT', payload: false })
					window.location.href = `${APP_ROOT}`;
				}
			}
		})
	}
}

export default getProject;
