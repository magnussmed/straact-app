const autosize = require('./webpack/utils/autosize');

module.exports = {
  plugins: [
    ['postcss-functions', {
      functions: {
        autosize: autosize
      }
    }],
    ['postcss-reporter', {
      clearReportedMessages: true
    }],
    ['postcss-import', {
      path: [
        './css/helpers',
        './css/basics',
        '../dist',
        './node_modules'
      ],
    }],
    'postcss-for',
    'postcss-percentage',
    'postcss-color-mod-function',
    'postcss-simple-vars',
    ['autoprefixer', {
      grid: true,
      overrideBrowserslist: ['last 2 versions', '> 5% in DK'],
    }],
    'postcss-nested',
    ['postcss-preset-env', {
      stage: 4,
      features: {
        'custom-media-queries': true,
        'nesting-rules': true,
      }
    }],
  ]
};
