module.exports = (env) => {
  'use strict';

  const path = require('path');

  let projectConfig = {};

  projectConfig.name = 'boilerplate-theme';
  projectConfig.url = projectConfig.name + '.test';

  projectConfig.assets = [];
  projectConfig.assets.push(
    path.resolve(process.cwd(), '..', 'css'),
    path.resolve(process.cwd(), '..', 'fonts'),
    path.resolve(process.cwd(), '..', 'img'),
  );

  /*
   * used to copy static assets into dist directory.
   */
  projectConfig.staticAssets = [];

  projectConfig.entries = {
    'base': path.resolve(__dirname, 'entries', 'base.js'),
  };

  return projectConfig;
};
