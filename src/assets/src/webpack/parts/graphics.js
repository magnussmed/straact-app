module.exports = (env) => {
  'use strict';

  const path = require('path');

  let graphics = {};
  let loaders = [];

  loaders.push(
    {
      loader: 'url-loader',
      options: {
        limit: (10 * 1024),
      },
    },
    {
      loader: 'svg-url-loader',
      options: {
        limit: (10 * 1024),
        noquotes: true,
      }
    },
    {
      loader: 'file-loader',
      options: {
        name: 'img/[name].[ext]',
      }
    }
  );

  if ( typeof env !== 'undefined' && env && env.production === true ) {
    loaders.push(
      {
        loader: 'image-webpack-loader',
        options: {}
      }
    );
  }

  graphics.rules = {
    test: /\.(gif|png|jpe?g|svg)$/i,
    use: loaders,
    include: [
      path.resolve(__dirname, '..', '..', 'img')
    ],
    exclude: [
      path.resolve(__dirname, '..', '..', 'icons' )
    ],
  };

  return graphics;
};
