module.exports = (env) => {
  'use strict';

  const path = require('path');

  let fonts = {};
  fonts.rules = {
    test: /\.(woff(2)?|ttf|otf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
    use: [{
      loader: 'file-loader',
      options: {
        name(file) {
          return (file.slice((process.cwd().length + 1))); // get relative path of font-file and remove preceding slash
        }
      }
    }],
    include: [
      path.resolve(__dirname, '..', '..', 'fonts')
    ],
    exclude: [
      path.resolve(__dirname, '..', '..', 'icons')
    ]
  };

  return fonts;
};
