/* Based on: https://css-tricks.com/linearly-scale-font-size-with-css-clamp-based-on-the-viewport/ & https://xgkft.csb.app/ */
const autosize = (viewMin, viewMax, sizeMin, sizeMax) => {

	let defaultRem = 16
	viewMin = viewMin / defaultRem;
	viewMax = viewMax / defaultRem;

	sizeMin = sizeMin / defaultRem;
	sizeMax = sizeMax / defaultRem;

	let slope = ( sizeMax - sizeMin ) / ( viewMax - viewMin )

	let yAxis = -viewMin * slope + sizeMin

	return `clamp(${ sizeMin }rem, calc(${ yAxis }rem + ${ slope * 100 }vw), ${ sizeMax }rem)`;
}

module.exports = autosize;
