import { SecondaryButton } from '../buttons/SecondaryButton';
import FilterDramaRoundedIcon from '@mui/icons-material/FilterDramaRounded';

import updateContent from '../../api/content/update-content';

const SaveContentButton = ( props ) => {
	const { content, setContent } = props;

	return (
		<SecondaryButton
			className="save-content__btn"
			text={"Save content"}
			icon={ <FilterDramaRoundedIcon />}
			onClick={ () => {
				updateContent( content, setContent )
			} }
		/>
	)
}

export default SaveContentButton;
