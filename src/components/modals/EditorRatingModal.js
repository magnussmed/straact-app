import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Rating from '@mui/material/Rating';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import Typography from '@mui/material/Typography';

import { PrimaryButton } from '../buttons/PrimaryButton';
import { SecondaryButton } from '../buttons/SecondaryButton';

import sendFeedback from '../../api/feedback/send-feedback';

const EditorRatingModal = ( props ) => {
	const user                                    = useSelector( state => state.user );
	const content                                 = useSelector( state => state.content );
	const loading                                 = useSelector( state => state.loading );
	const dispatch                                = useDispatch();
	const [ open, setOpen ]                       = useState( false );
	const [ ratingQuestions, setRatingQuestions ] = useState( [
		{
			'question': 'How would you rate working with our editor?',
			'answer': ''
		},
		{
			'question': 'How satisfied are you with the AI-written text?',
			'answer': ''
		},
		{
			'question': 'Additional comments: What could be improved?',
			'answer': ''
		}
	] );

	const handleOpen = () => {
		setOpen( true );
	}

	const handleClose = () => {
		setOpen( false );
	}

	const style = {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 400,
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: 24,
		pt: 2,
		px: 4,
		pb: 3,
	};

	const onSubmit = ( event ) => {
		event.preventDefault();

		const sendFeedbackThunk = sendFeedback(
			{
				'uid': user?.uid,
				'action': 'editor_rating',
				'other_identifiers': {
					'content_id': content?.id
				},
				'comment': ratingQuestions
			},
			setOpen
		)

		sendFeedbackThunk( dispatch )
	}


	let button = <PrimaryButton text={ props.t( 'Send feedback' ) } type="submit" />

	return (
		<>
			<SecondaryButton
				className="editor-rating__btn"
				icon={ <FavoriteIcon /> }
				text={ props.t( 'Give us feedback' ) }
				size="small"
				onClick={ handleOpen }
			/>
			<Modal
				className="basic__modal editor-rating__modal"
				open={open}
				onClose={handleClose}
			>
				<Box
					className="basic__modal-box editor-rating__modal-box"
					sx={{ ...style, width: '100%', maxWidth: '600px' }}
					disabled={ loading }
				>
					<div className="basic__modal-title editor-rating__modal-title">{ props.t( 'Give us feedback' ) }</div>
					<div className="basic__modal-description">{ props.t( 'We use the feedback to improve our service' ) }</div>

					<form
						className="sign-up__form form"
						onSubmit={ onSubmit }
					>
						<div className="form__fields">
							<Box>
								<Typography component="legend">{ props.t( ratingQuestions[0].question ) }</Typography>
								<Rating
									name="customized-color"
									defaultValue={2}
									getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
									precision={0.5}
									icon={<FavoriteIcon fontSize="inherit" />}
									emptyIcon={<FavoriteBorderIcon fontSize="inherit" />}
									onChange={ event => {
										let newRatingQuestions = ratingQuestions.map((obj, key) => key === 0 ? { ...obj, answer: event.target.value } : obj );
										setRatingQuestions( newRatingQuestions )
									} }
								/>
							</Box>
							<Box>
								<Typography component="legend">{ props.t( ratingQuestions[1].question ) }</Typography>
								<Rating
									name="customized-color"
									defaultValue={2}
									getLabelText={(value) => `${value} Heart${value !== 1 ? 's' : ''}`}
									precision={0.5}
									icon={<FavoriteIcon fontSize="inherit" />}
									emptyIcon={<FavoriteBorderIcon fontSize="inherit" />}
									onChange={ event => {
										let newRatingQuestions = ratingQuestions.map((obj, key) => key === 1 ? { ...obj, answer: event.target.value } : obj );
										setRatingQuestions( newRatingQuestions )
									} }
								/>
							</Box>
							<TextField
								label={ props.t( ratingQuestions[2].question ) }
								multiline
								variant="outlined"
								rows={4}
								maxRows={4}
								autoComplete="new-name"
								onChange={ event => {
									let newRatingQuestions = ratingQuestions.map((obj, key) => key === 2 ? { ...obj, answer: event.target.value } : obj );
									setRatingQuestions( newRatingQuestions )
								} }
							/>
							{ button }
						</div>
					</form>
				</Box>
			</Modal>
		</>
	)
}

export default EditorRatingModal;
