import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

const DisplayError = ( { display_message, snackbarHandleClose, t } ) => {
	return (
		<Snackbar open={ true } autoHideDuration={ 30000 } onClose={ snackbarHandleClose } anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}>
			<Alert onClose={snackbarHandleClose} severity="error" sx={{ width: '100%' }}>
				{ t( display_message?.reason ) }
			</Alert>
		</Snackbar>
	)
}

export default DisplayError;
