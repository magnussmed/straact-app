import axios from 'axios';

import { API_ENDPOINTS, ACCESS_TOKEN } from '../../../utils/constants';


class RewriteCommandTool {
	static get isInline() {
		return true;
	}

	static get title() {
		return 'Rewrite';
	}

	get state() {
		return this._state;
	}

	set state( state ) {
		this._state = state

		this.button.classList.toggle(this.api.styles.inlineToolButtonActive, state);
	}

	constructor({api, config}) {
		this.api = api;
		this.config = config || {};
		this.button = null;
		this._state = false;

		this.tag = 'P';
		this.class = 'cdx-custom-command-response';
	}

	render() {
		this.button = document.createElement('button');
		this.button.type = 'button';
		this.button.innerHTML = '<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-vubbuv" focusable="false" aria-hidden="true" viewBox="0 0 24 24" data-testid="AutoFixHighRoundedIcon"><path d="m20.45 6 .49-1.06L22 4.45c.39-.18.39-.73 0-.91l-1.06-.49L20.45 2c-.18-.39-.73-.39-.91 0l-.49 1.06-1.05.49c-.39.18-.39.73 0 .91l1.06.49.49 1.05c.17.39.73.39.9 0zM8.95 6l.49-1.06 1.06-.49c.39-.18.39-.73 0-.91l-1.06-.48L8.95 2c-.17-.39-.73-.39-.9 0l-.49 1.06-1.06.49c-.39.18-.39.73 0 .91l1.06.49L8.05 6c.17.39.73.39.9 0zm10.6 7.5-.49 1.06-1.06.49c-.39.18-.39.73 0 .91l1.06.49.49 1.06c.18.39.73.39.91 0l.49-1.06 1.05-.5c.39-.18.39-.73 0-.91l-1.06-.49-.49-1.06c-.17-.38-.73-.38-.9.01zm-1.84-4.38-2.83-2.83a.9959.9959 0 0 0-1.41 0L2.29 17.46c-.39.39-.39 1.02 0 1.41l2.83 2.83c.39.39 1.02.39 1.41 0L17.7 10.53c.4-.38.4-1.02.01-1.41zm-3.5 2.09L12.8 9.8l1.38-1.38 1.41 1.41-1.38 1.38z"></path></svg>';
		this.button.classList.add(this.api.styles.inlineToolButton);

		return this.button;
	}

	surround(range) {
		this.wrap(range);
	}

	wrap(range) {
		this.config.dispatch({ type: 'SET_IS_LOADING', payload: true })
		const selectedText = range.toString();
		const mark = document.createElement(this.tag);

		console.log('selectedText', selectedText, 'range', range)

		console.log('this.config.', this.config)

		let params = {...this.config.params, query: { ...this.config.params.query, input: selectedText } }

		axios.post(
			`${API_ENDPOINTS.STRAACT_FLASK_API}/content/write`,
			params,
			{
				headers: {
					"Authorization" : `Bearer ${ACCESS_TOKEN}`
				}
			}
		)
		.then( response => {
			if ( response?.data ) {
				if ( response?.data?.type === 'success' ) {
					mark.classList.add(this.class);
					let len = response?.data?.content?.query.length;
					console.log('response?.data?.content', response?.data?.content);
					console.log("TEST:", response?.data?.content?.query[len - 1]);
					mark.innerHTML = response?.data?.content?.query[len - 1]?.items?.paragraph?.items[0]?.result;
					range.collapse(false)
					range.insertNode(mark);
				}

				if ( response?.data?.type === 'error' ) {
					this.config.dispatch({ type: 'DISPLAY_ERROR', payload: response.data })
				}

				this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
			}
		}).catch( error => {
			this.config.dispatch({ type: 'DISPLAY_ERROR', payload: error.response.data })
			this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
		})
	}

	checkState() {}
}

export default RewriteCommandTool;