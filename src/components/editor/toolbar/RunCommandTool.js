import axios from 'axios';

import { API_ENDPOINTS, ACCESS_TOKEN } from '../../../utils/constants';


class RunCommandTool {
	static get isInline() {
		return true;
	}

	static get title() {
		return 'Run command';
	}

	get state() {
		return this._state;
	}

	set state( state ) {
		this._state = state

		this.button.classList.toggle(this.api.styles.inlineToolButtonActive, state);
	}

	constructor({api, config}) {
		this.api = api;
		this.config = config || {};
		this.button = null;
		this._state = false;

		this.tag = 'P';
		this.class = 'cdx-custom-command-response';
	}

	render() {
		this.button = document.createElement('button');
		this.button.type = 'button';
		this.button.innerHTML = '<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium MuiBox-root css-uqopch" focusable="false" aria-hidden="true" viewBox="0 0 24 24" data-testid="PlayCircleOutlineRoundedIcon"><path d="m10.8 15.9 4.67-3.5c.27-.2.27-.6 0-.8L10.8 8.1c-.33-.25-.8-.01-.8.4v7c0 .41.47.65.8.4zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"></path></svg>';
		this.button.classList.add(this.api.styles.inlineToolButton);

		return this.button;
	}

	surround(range) {
		this.wrap(range);
	}

	wrap(range) {
		this.config.dispatch({ type: 'SET_IS_LOADING', payload: true })

		const selectedText = range.toString();
		const mark = document.createElement(this.tag);

		let params = {...this.config.params, query: { ...this.config.params.query, command: selectedText } }

		axios.post(
			`${API_ENDPOINTS.STRAACT_FLASK_API}/content/write`,
			params,
			{
				headers: {
					"Authorization" : `Bearer ${ACCESS_TOKEN}`
				}
			}
		)
		.then( response => {
			if ( response?.data ) {
				if ( response?.data?.type === 'success' ) {
					mark.classList.add(this.class);
					let len = response?.data?.content?.query.length;
					mark.innerHTML = response?.data?.content?.query[len - 1]?.items?.paragraph?.items[0]?.result;
					range.collapse(false)
					range.insertNode(mark);
				}

				if ( response?.data?.type === 'error' ) {
					this.config.dispatch({ type: 'DISPLAY_ERROR', payload: response.data })
				}

				this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
			}
		}).catch( error => {
			this.config.dispatch({ type: 'DISPLAY_ERROR', payload: error.response.data })
			this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
		})
	}

	checkState() {}
}

export default RunCommandTool;