import axios from 'axios';

import { API_ENDPOINTS, ACCESS_TOKEN } from '../../../utils/constants';


class ExtendCommandTool {
	static get isInline() {
		return true;
	}

	static get title() {
		return 'Extend';
	}

	get state() {
		return this._state;
	}

	set state( state ) {
		this._state = state

		this.button.classList.toggle(this.api.styles.inlineToolButtonActive, state);
	}

	constructor({api, config}) {
		this.api = api;
		this.config = config || {};
		this.button = null;
		this._state = false;

		console.log('CONFIG', config)

		this.tag = 'P';
		this.class = 'cdx-custom-command-response';
	}

	render() {
		this.button = document.createElement('button');
		this.button.type = 'button';
		this.button.innerHTML = '<svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-i4bv87-MuiSvgIcon-root" focusable="false" aria-hidden="true" viewBox="0 0 24 24" data-testid="NotesRoundedIcon"><path d="M20 11H4c-.55 0-1 .45-1 1s.45 1 1 1h16c.55 0 1-.45 1-1s-.45-1-1-1zM4 18h10c.55 0 1-.45 1-1s-.45-1-1-1H4c-.55 0-1 .45-1 1s.45 1 1 1zM20 6H4c-.55 0-1 .45-1 1v.01c0 .55.45 1 1 1h16c.55 0 1-.45 1-1V7c0-.55-.45-1-1-1z"></path></svg>';
		this.button.classList.add(this.api.styles.inlineToolButton);

		return this.button;
	}

	surround(range) {
		this.wrap(range);
	}

	wrap(range) {
		this.config.dispatch({ type: 'SET_IS_LOADING', payload: true })
		const selectedText = range.toString();
		const mark = document.createElement(this.tag);

		console.log('selectedText', selectedText, 'range', range)

		console.log('this.config.', this.config)

		let params = {...this.config.params, query: { ...this.config.params.query, input: selectedText } }

		axios.post(
			`${API_ENDPOINTS.STRAACT_FLASK_API}/content/write`,
			params,
			{
				headers: {
					"Authorization" : `Bearer ${ACCESS_TOKEN}`
				}
			}
		)
		.then( response => {
			if ( response?.data ) {
				if ( response?.data?.type === 'success' ) {
					mark.classList.add(this.class);
					let len = response?.data?.content?.query.length;
					console.log('response?.data?.content', response?.data?.content);
					console.log("TEST:", response?.data?.content?.query[len - 1]);
					mark.innerHTML = response?.data?.content?.query[len - 1]?.items?.paragraph?.items[0]?.result;
					range.collapse(false)
					range.insertNode(mark);
				}

				if ( response?.data?.type === 'error' ) {
					this.config.dispatch({ type: 'DISPLAY_ERROR', payload: response.data })
				}

				this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
			}
		}).catch( error => {
			this.config.dispatch({ type: 'DISPLAY_ERROR', payload: error.response.data })
			this.config.dispatch({ type: 'SET_IS_LOADING', payload: false })
		})
	}

	checkState() {}
}

export default ExtendCommandTool;