import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';

import logo from '../../../assets/dist/img/shopify-icon.svg';
import { PrimaryButton } from '../../buttons/PrimaryButton';
import IntegrationInstructionsRoundedIcon from '@mui/icons-material/IntegrationInstructionsRounded';
import Switch from '@mui/material/Switch';

const ConnectShopify = ( props ) => {
	const user = useSelector( state => state.user );

	const [checked, setChecked] = useState( false );

	const handleChange = ( e ) => {
		setChecked( e.target.checked );
	}

	if ( ! user ) {
		return ''
	}

	return (
		<div className="integrations-connect__item">
			<div className="integrations-connect__item-header">
				<div className="integrations-connect__item-icon">
					<img src={ logo } />
				</div>
				<div className="integrations-connect__item-title">
					{ props.t( 'Shopify' ) }
				</div>

				<Switch
					checked={checked}
					onChange={handleChange}
					inputProps={{ 'aria-label': 'controlled' }}
				/>
			</div>
			<div className="integrations-connect__item-content">
				<div className="integrations-connect__item-description">
					{ props.t( 'Write a meaningful description' ) }
				</div>
			</div>
			<div className="integrations-connect__item-setup">
				<div className="integrations-connect__item-view">
					<NavLink to="/integrations/shopify">{ props.t( 'Manage integration' ) }</NavLink>
				</div>
			</div>
		</div>
	)
}

export default ConnectShopify;
