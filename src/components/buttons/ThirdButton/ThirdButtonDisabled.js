import Button from '@mui/material/Button';

const ThirdButtonDisabled = ( props ) => {
	return (
		<>
			<Button
				startIcon={ props.icon }
				variant="contained"
				disabled
				size="small"
				className={ `btn third-btn ${props.className}` }
			>
				{ props.text }
			</Button>
		</>
	)
}

export default ThirdButtonDisabled;