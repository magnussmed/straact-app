import Moment from 'react-moment';
import 'moment-timezone';

const BeautyDatetime = ( { datetime } ) => {
    return <Moment tz="Europe/Copenhagen" fromNow>{ datetime }</Moment>
}

export default BeautyDatetime;
