const backofficeUsersReducer = ( state = false, action ) => {
	switch( action.type ) {
		case 'BACKOFFICE_GET_USERS':
			return action.payload
		default:
			return state
	}
}

export default backofficeUsersReducer;