const stripeSubscriptionReducer = ( state = false, action ) => {
	switch( action.type ) {
		case 'GET_SUBSCRIPTION':
			return action.payload
		default:
			return state
	}
}

export default stripeSubscriptionReducer;
