const associatedContentReducer = ( state = false, action ) => {
	switch( action.type ) {
		case 'GET_ASSOCIATED_CONTENT':
			return action.payload
		default:
			return state
	}
}

export default associatedContentReducer;