const associatedProjectsReducer = ( state = false, action ) => {
	switch( action.type ) {
		case 'GET_ASSOCIATED_PROJECTS':
			return action.payload
		default:
			return state
	}
}

export default associatedProjectsReducer;