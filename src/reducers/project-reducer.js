const projectReducer = ( state = false, action ) => {
	switch( action.type ) {
		case 'GET_PROJECT':
			return action.payload
		default:
			return state
	}
}

export default projectReducer;