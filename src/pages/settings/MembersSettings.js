import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

import LeftPanel from '../../layouts/LeftPanel';
import TopPanel from '../../layouts/TopPanel';
import SettingsMenu from '../../components/settings/SettingsMenu';
import { SecondaryButton, SecondaryButtonDisabled } from '../../components/buttons/SecondaryButton';
import updateUser from '../../api/user/update-user';
import { SetLocalLanguage } from '../../utils/Language';
import OrganizationMembers from '../../components/settings/members/OrganizationMembers';

const MembersSettings = ( props ) => {
	const user = useSelector( state => state.user );

	useEffect(() => {
		props.setAppClasses( 'settings' )
	}, []);

	return (
		<>
			<LeftPanel { ...props } />

			<div className="right-panel">
				<TopPanel
					title={{
						'value': props.t( 'Settings' ),
						'editable': false
					}}
					description={{
						'value': props.t( "Change the company settings" ),
						'editable': false,
					}}
				/>

				<SettingsMenu {...props} />

				<div className="settings-sections">
					<div className="settings-sections__item">
						<OrganizationMembers {...props} />
					</div>
				</div>
			</div>
		</>
	)
}

export default MembersSettings;
