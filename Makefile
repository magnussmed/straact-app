REPO        = $(shell basename -s .git `git config --get remote.origin.url`)
CUR_BRANCH  = $(shell git rev-parse --abbrev-ref HEAD)

build-prod:
	rm -rf build
	npm run build:webpack
	npm run build:react

deploy:
ifeq ($(e), prod)
	@echo "\033[0;32mStarting deploy to production...\033[0m"
	firebase use default
	firebase deploy

else
	@echo "\033[0;32mStarting deploy to staging...\033[0m"
	firebase use staging
	firebase deploy

endif

import-db-from-prod:
	gcloud config set project bold-apricot-165517
	gcloud firestore export gs://straact-prod-database-dumps --async
	gcloud transfer jobs create gs://straact-prod-database-dumps gs://straact-database-dumps --overwrite-when=always --delete-from=destination-if-unique
	gcloud config set project straact-stage-a027b
	gcloud firestore import $(shell gsutil ls -l gs://straact-database-dumps/ | sort -V | tail -n 1 | sed 's/ //g')